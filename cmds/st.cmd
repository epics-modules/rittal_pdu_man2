# This should be a test startup script
#!/usr/bin/env iocsh.bash
require rittal_pdu_man2
# -----------------------------------------------------
# Configure Environment
# -----------------------------------------------------
# Default paths to locate database and protocol
# epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(rittalpduman2_DIR)db/")
epicsEnvSet("IP_ADDRESS","172.30.235.16:502")
epicsEnvSet("PORT", "502")
epicsEnvSet("PREFIX", "P:")
epicsEnvSet("P", "LabS-EMBLA:")
epicsEnvSet("R", "Chop-PDU-00:")
epicsEnvSet("TCPPORT_NAME", "Chopper")
epicsEnvSet("MBSLAVE_ADDR", "1")

iocshLoad("$(rittal_pdu_man2_DIR)/rittalPDUMan2.iocsh", "IP_ADDRESS=$(IP_ADDRESS), TCPPORT_NAME=$(TCPPORT_NAME), MBSLAVE_ADDR=$(MBSLAVE_ADDR), P=$(P), R=$(R)")
